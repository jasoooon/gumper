<?php

include('bootstrap.php');

startResponse();

requireAuthKey();

if(isset($_REQUEST['scriptPath'])){
  $scriptPath = $_REQUEST['scriptPath'];
} else {
	appendResponse('error','A script was not specified');
	die(sendResponse());
}

addScriptToRegistry();
sendResponse();

?>