<?php

require_once('bootstrap.php');

requireAuthKey();

if(isset($_REQUEST['scriptPath']) || isset($_FILES['file'])){
  if(isset($_REQUEST['scriptPath'])){ $scriptPath = $_REQUEST['scriptPath']; }
  if(isset($_FILES['file'])){ $scriptFile = $_FILES['file']; }
} else { die('A script was not specified'); }

//This is a GUI request, not API
$gui = true;

//If a script is being uploaded, then run upload-script.php as well. Otherwise, skip this step.
if(isset($scriptFile) && !empty($scriptFile['name'])){

	include('./upload-script.php');

} else {

	if(file_exists($scriptPath)){
		$scripts = json_decode(file_get_contents('scripts.psrconfig'),true);

		$scriptMatch = 0;
	
		foreach($scripts['scripts'] as $script){
			if($script['script'] == $scriptPath){
				$scriptMatch++;
			}
		}
	
		if($scriptMatch > 0){
		die('The specified script already exists in the script registry.');
		} else {
			$scriptId = str_replace(".","",uniqid(null,true)) . rand(100,999);
				$newScript = array('id' => $scriptId, 'script' => $scriptPath, 'scriptType' => $_REQUEST['scriptType']);
				$scripts['scripts'][] = $newScript;
				file_put_contents('scripts.psrconfig',json_encode($scripts));

				header("Location: index.php?script=$scriptId&authKey=$authKey");
		}
	} else { die('The script specified could not be found on this system'); }

}

?>