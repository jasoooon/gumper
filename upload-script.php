<?php

include('bootstrap.php');

startResponse();

requireAuthKey();

if($allowScriptUpload != true){
	appendResponse('error','Script uploads are not enabled');
	die(sendResponse());
}

if(isset($_FILES['file'])) {
    
    $filename = $_FILES['file']['name'];
    $source = $_FILES['file']['tmp_name'];
    $type = $_FILES['file']['type'];
  	
  	if(isset($source) && isset($filename)){
  		$destination = "$scriptUploadPath/$filename";
  		
  		//Make sure the type of script is valid based on registered script types
  		$scriptTypes = json_decode(file_get_contents('scriptTypes.psrconfig'),1);
  		$fileinfo = pathinfo($destination);
  		$scriptTypeMatch = false;
  		
  		//If script type was specified, use this to determine script type
  		if(isset($_REQUEST['scriptType'])){
  			$scriptType = $_REQUEST['scriptType'];
			
				foreach($scriptTypes['scriptTypes'] as $st){
					if($scriptType == $st['name']){ $scriptTypeMatch = true; }
				}
  		}
  		//Otherwise, validate script type based on extension
  		if(isset($fileinfo['extension']) && $scriptTypeMatch != true){
				$extension = $fileinfo['extension'];
			
				foreach($scriptTypes['scriptTypes'] as $st){
					if($extension == $st['extension']){ $scriptTypeMatch = true; }
				}
			}
			if(!$scriptTypeMatch == true){
				appendResponse('error', 'The script supplied could not be matched to a valid script type');
				die(sendResponse());
			}
  		
  		if(!file_exists($destination)){
  			move_uploaded_file($source,"$scriptUploadPath/$filename");
  			if(file_exists($destination)){
  				appendResponse('response', 'Script was uploaded successfully');
  				
  				//Register the script
  				$scriptPath = $destination;
  				addScriptToRegistry();
  				
  				
  			} else {
  				appendResponse('error', 'An error prevented the script from being uploaded');
  			}
  		} else{
  			appendResponse('error','A script with this file name already exists');
  			die(sendResponse());
  		} 
  	} else {
  		appendResponse('error','A valid file was not supplied');
  		die(sendResponse());
  	}
  	       
} else { appendResponse('error','A file was not provided'); }

if(isset($gui) && $gui == true){
	header("Location: index.php?script=$scriptId&authKey=$authKey");
} else { sendResponse(); }

?>