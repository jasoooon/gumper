<?php

include('bootstrap.php');

requireAuthKey();

?>

<html>

<head>

	<script src="foundation/js/vendor/jquery.js" type="text/javascript"></script>
	<script src="foundation/js/foundation.min.js" type="text/javascript"></script>
	<script src="foundation/js/vendor/modernizr.js"></script>
	<link rel="stylesheet" type="text/css" href="foundation/css/foundation.min.css" />

</head>

<body>

			<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
			<script>

				var app = angular.module('GetScripts', []);
				app.controller('GetScriptsCtrl', function($scope, $http) {
					$http.get("get-scripts.php?authKey=<?php echo $authKey; ?>")
					.success(function(response) {$scope.scripts = response;});
				});

				function confirmDelete(script){
					conf = confirm("Click OK to remove this script (" + script + ") from the registry. The file will not be deleted.");
					if(conf == true){window.location.assign("remove-script.php?authKey=<?php echo $authKey; ?>&script=" + script);}
				}

				var app = angular.module('GetScriptTypes', []);
				app.controller('GetScriptTypesCtrl', function($scope, $http) {
					$http.get("get-scripttypes.php?authKey=<?php echo $authKey; ?>")
					.success(function(response) {$scope.response = response;});
				});

			</script>

			<style scoped>
			</style>

			<center><h1>Gumper</h1></center>

			<form method="POST" enctype="multipart/form-data" action="add-script-submit.php" class="row">
				<h2>Add scripts</h2>
				<p><label>Local script path (e.g.: C:\scripts\myscript.ps1):</label></p>
				<p><input type="text" name="scriptPath" class="large-4 columns" />
        <?php if ($allowScriptUpload == true): ?>
        <p><label>Or, upload a script:</label></p>
        <input type="file" name="file" /><br />
        <?php endif; ?>
				<p><label>Script type:</label></p>
				<div ng-app="GetScriptTypes">
					<select ng-controller="GetScriptTypesCtrl" name="scriptType">
					  <option selected value="">Unspecified (choose at run-time based on extension)</option>
						<option ng-repeat="t in response.scriptTypes.scriptTypes | orderBy:'-name'">{{ t.name }}</option>
					</select>
				</div>
				<input type="submit" class="button" value="Add this script" /></p>
				<p><input type="hidden" value="<?php echo $authKey; ?>" name = "authKey" /></p>
			</form>

			<div ng-app="GetScripts" class="row" id="GetScriptsModule">

				<h2>Available scripts</h2>

				<table role="grid" ng-controller="GetScriptsCtrl">
					<tr>
						<th>Script ID</th>
						<th>Script Path</th>
						<th>Type</th>
						<th></th>
					</tr>
					<tr ng-repeat="s in scripts.scripts | orderBy:'-id'">
						<td>{{ s.id }}</td>
						<td>{{ s.script }}</td>
						<td>{{ s.scriptType }}</td>
						<td><button onClick="confirmDelete(event.target.value);" value="{{ s.id }}">Remove</button></td>
					</tr>
				</table>

			</div>

			<script>angular.bootstrap(document.getElementById('GetScriptsModule'),['GetScripts']);</script>

</body>

</html>
