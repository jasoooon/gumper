<?php

//Slightly modified from http://code.stephenmorley.org/php/sending-files-using-curl/

//This is an example of how a script might be uploaded via API. Uncomment to run.

/*
// initialise the curl request
$request = curl_init('https://localhost/upload-script.php');

// send a file
curl_setopt($request, CURLOPT_POST, true);
curl_setopt(
    $request,
    CURLOPT_POSTFIELDS,
    array(
      'file' => '@' . realpath('./test-script.sh'),
      'authKey' => 'DefaultChangeMePlease!'
    ));

// output the response
curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
echo curl_exec($request);

// close the session
curl_close($request);

*/

?>