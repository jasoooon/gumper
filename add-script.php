<?php

if(isset($_REQUEST['authKey'])){
  $authKey = $_REQUEST['authKey'];
} else { die('You must provide an auth key.'); }

$keys = json_decode(file_get_contents('keys.psrconfig'),true);

foreach($keys['keys'] as $key){
  if($authKey == $key['value']){
    $matchKey = true;
  }
}

if(!$matchKey){ die('A valid auth key was not provided.'); }

?>

<html>

<head>

  <title>Add new scripts</title>

</head>

<body>

  <form method="GET" action="add-script-submit.php">
    <p><label>Script path (e.g.: C:\scripts\myscript.ps1):</label> <input type="text" name="scriptPath" /></p>
    <p><input type="submit" value="Add this script" /></p>
  </form>

<?php

$scripts = json_decode(file_get_contents('scripts.psrconfig'),true);

?>

  <table>
    <tr><th>Script ID</th><th>Script Path</th></tr>
	<?php foreach ($scripts['scripts'] as $script){ echo "<tr><td>" . $script['id'] . "</td><td>" . $script['script'] . "</td></tr>\n"; } ?>
  </table>
  
</body>

</html>