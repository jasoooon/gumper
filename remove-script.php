<?php

include('bootstrap.php');

startResponse();

requireAuthKey();

requireScript();

$script = $_REQUEST['script'];

$scripts = json_decode(file_get_contents('scripts.psrconfig'),true);

//Remove script from array
$i = 0;
foreach($scripts['scripts'] as $sc){

  if($script == $sc['id']){
    $removeIndex =  $i;
    break;
  }
  $i++;
}

unset($scripts['scripts'][$removeIndex]);

$newScripts = array_values($scripts['scripts']);

unset($scripts['scripts']);

$scripts['scripts'] = $newScripts;

saveScripts($scripts);

header("Location: index.php?script=$scriptId&authKey=$authKey");

?>
