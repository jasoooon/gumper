<?php

include('bootstrap.php');

startResponse();

requireAuthKey();

$scripts = getScripts();

appendResponse('scripts',$scripts);

sendResponse();

?>