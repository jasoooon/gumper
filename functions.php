<?php

function startResponse(){

	$GLOBALS['response'] = array('authentication' => '', 'response' => '');

}

function sendResponse(){

	echo(json_encode($GLOBALS['response']));

}

function appendResponse($attr,$val){

	$GLOBALS['response'][$attr] = $val;

}

function requireAuthKey(){
	
	global $authKey;
	
	//Check to see if an authentication key was provided, die if not
	if(isset($_REQUEST['authKey'])){
	  $authKey = $_REQUEST['authKey'];
	} else {

	  appendResponse('authentication','failed');
	  appendResponse('error','Connection refused: An auth key was not provided.');
	  die(sendResponse());

	}

	//Get list of all keys
	$authKeys = getKeys();
	
	if(keyValid($authKeys,$authKey)){
		appendResponse('authentication','success');
	} else {
		appendResponse('authentication','failed');
		appendResponse('error','Connection refused: The auth key provided was not valid.');
		die(sendResponse());
	}
	
}

function getKeys(){

	$authKeys = json_decode(file_get_contents('keys.psrconfig'),true);
	return($authKeys['keys']);

}

function keyValid($keys,$authKey){

	$authKeyMatch = false;

	//Search key array for a matching auth key
	foreach($keys as $key){
		if($key['value'] == $authKey){
		$authKeyMatch = true;
		}
	}

	return($authKeyMatch);
	
}

function requireScript(){
	
	//Check to see if a script provided, die if not
	if(isset($_REQUEST['script'])){
	  $script = $_REQUEST['script'];
	} else {
	  appendResponse('error','No action to perform: A script was not provided.');
	  die(sendResponse());
	}

	//Get list of all scripts
	$scripts = getScripts();
	
	if(scriptValid($scripts,$script)){
		appendResponse('script',$script);
	} else {
		appendResponse('error','Script not found: The provided script could not be found.');
		die(sendResponse());
	}
	
}

function getScripts(){

	$scripts = json_decode(file_get_contents('scripts.psrconfig'),true);
	return($scripts['scripts']);

}

function getScriptTypes(){

	$scriptTypes = json_decode(file_get_contents('scriptTypes.psrconfig'),true);
	return($scriptTypes);

}

function getScriptLaunchConfig($script){

	$types = getScriptTypes();
	foreach($types['scriptTypes'] as $t){
		if(isset($script['scriptType']) && $script['scriptType'] == $t['name']){
			return($t);
		}
		else{
			$ext = pathinfo($script['script'], PATHINFO_EXTENSION);
			if($t['extension'] == $ext){
				return($t);
			}
		}
	}

}

function scriptValid($scripts,$checkScript){

	$scriptMatch = false;

	//Search key array for a matching script
	foreach($scripts as $sc){
		if($sc['id'] == $checkScript){
		$scriptMatch = true;
		global $script;
		$script = $sc;
		}
	}

	return($scriptMatch);
	
}

function runScript($script,$type,$args = ''){

	
	$scriptFile = $script['script'];
	
	//Accept arguments as a list separated by semicolons. Convert this to a string - maybe unnecessary.
	if($args){
		$argsArray = explode(";",$args);
		$argString = "";
		foreach($argsArray as $arg){
			$argString = $argString . $arg . " ";
			}
	} else { $argString = $args; }

	//Construct the command to run in shell_exec using the script type's settings
	$prefix = $type['launchPrefix'];
	$suffix = $type['launchSuffix'];
	
	//Run the script... Yay!
	$response = (shell_exec("$prefix $scriptFile $argString $suffix"));
	
	//To help avoid excess text, only return output between [[OUTPUT]] and [[/OUTPUT]] tags - not necessary
	//but helpful when finding it difficult to suppress unwanted output.
	$response = preg_replace("/.*\[\[OUTPUT\]\]/s","",$response);
	$response = preg_replace("/\[\[\/OUTPUT\]\].*/s","",$response);
	
	//Remove white space from both ends
	$response = trim($response);
	
	//Turn off xml errors while we determine format
	libxml_use_internal_errors(true);
	//Is it json?
	if(json_decode($response)){
		$returnResponse = json_decode($response);
	} 
	//Is it xml?
	elseif(simplexml_load_string($response)){
	  libxml_use_internal_errors(false);
		$returnResponse = simplexml_load_string($response);
	}
	//Must be plain text or something I don't understand
	else { $returnResponse = $response; }
	
	appendResponse('scriptResult',$returnResponse);

}

function saveScripts($scripts){

	file_put_contents('scripts.psrconfig',json_encode($scripts));

}

function logActivity(){
	global $logFile;
	
	$date = date('Y-m-d H:i:s');
	
	global $_REQUEST;
	
	//Log the authKey if used
	if(isset($_REQUEST['authKey'])){
		$authKey = $_REQUEST['authKey'];
		} else {
			$authKey = '---';
		}
	
	//Log the script ID if specified
	if(isset($_REQUEST['script'])){
		$script = $_REQUEST['script'];
		} else {
			$script = '---';
		}
	
	//Log script arguments if specified
	if(isset($_REQUEST['args'])){
		$args = $_REQUEST['args'];
		} else {
			$args = '---';
		}
	
	global $_SERVER;
	
	$host = $_SERVER['SERVER_NAME'];
	
	//Get IP address, accounting for proxies
	if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = $_SERVER['REMOTE_ADDR'];
	}
	
	//If this request was sent via proxy, log the original host and the proxy server
	if(isset($_SERVER['HTTP_X_FORWARDED_SERVER'])){ $ip = $_SERVER['HTTP_X_FORWARDED_SERVER'] . ' -> ' . $ip; }
	if(isset($_SERVER['HTTP_X_FORWARDED_HOST'])){ $ip = $_SERVER['HTTP_X_FORWARDED_HOST'] . ' -> ' . $ip; }
	
	$logString = "$ip $host " . $_SERVER['PHP_SELF'] . " $authKey [$date] $script $args";
	if($logFile){
		file_put_contents($logFile,$logString . PHP_EOL,FILE_APPEND);
	}
}

function addScriptToRegistry(){
  
  global $_REQUEST;
  global $scriptPath;

	if(file_exists($scriptPath)){
  
		$scripts = json_decode(file_get_contents('scripts.psrconfig'),true);

		$scriptMatch = 0;
	
		if($scripts['scripts']){
			foreach($scripts['scripts'] as $script){
			if($script['script'] == $scriptPath){
				$scriptMatch++;
			}
			}
		} else { $scripts = array("scripts" => array()); }
	
		if($scriptMatch > 0){
		appendResponse('error','The specified script already exists in the script registry.');
		die(sendResponse());
		} else {
			$scriptId = str_replace(".","",uniqid(null,true)) . rand(100,999);
			if(isset($_REQUEST['scriptType'])){ $scriptType = $_REQUEST['scriptType']; } else { $scriptType = ''; }
			$newScript = array('id' => $scriptId, 'script' => $scriptPath, 'scriptType' => $scriptType);
			$scripts['scripts'][] = $newScript;
			file_put_contents('scripts.psrconfig',json_encode($scripts));
			appendResponse('scriptID',$scriptId);
			appendResponse('result','success');
		}
	
	} else {
		appendResponse('error','The script specified could not be found on this system');
		die(sendResponse());
	}

}

?>