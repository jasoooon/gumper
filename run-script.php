<?php

include('bootstrap.php');

startResponse();

requireAuthKey();

requireScript();

if(isset($_REQUEST['args'])){
	$args = $_REQUEST['args'];
} else { $args = ''; }

$type = getScriptLaunchConfig($script);

if($type){
	runScript($script,$type,$args);
} else {
	appendResponse('error','Could not determine the script type based on supplied type name or file extension');
	die(sendResponse());
}

sendResponse();

?>