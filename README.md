# README #

Gumper is an API of sorts for executing PowerShell and other types of scripts remotely. It uses keys and script IDs to add security and returns results in json for web optimization. Gumper has been tested on Apache and IIS. 
Read the project wiki (https://bitbucket.org/jasoooon/gumper/wiki) for more details.

Gumper does not magically give you the ability to run PowerShell in a Linux environment or run Node.js where it hasn't been installed. Gumper merely accesses the command line of the server it runs on and executes commands. In other words, if you want to run a PowerShell script, install Gumper on a Windows machine. If you want to run a Node.js script, install Node.js on the server first.

### What is this repository for? ###

* Gumper holds a registry of scripts and runs them when requested, returning results as a json object.
* Version 0.1.0

### How do I get set up? ###

* If using IIS, be sure to install PHP for IIS (https://www.microsoft.com/web/platform/phponwindows.aspx or http://php.net/manual/en/install.windows.iis7.php)
* Copy files into the desired directory
* Edit or replace the default authKey in keys.psrconfig
* For PowerShell, modify the execution policy in scripttypes.psrconfig. Default is "Bypass" for the easiest/riskiest setup, but you might want to look at RemoteSigned or something else.
* Navigate to index.php?authKey=yournewauthkey to start adding/removing scripts via GUI
* Gumper now (as of v. 0.1.0) uploads script as well, but $allowScriptUpload must be changed to "true" in config.php (see examples/test-upload.php for API example)
* Run scripts via run-script.php?authKey=yournewauthkey&script=idofscriptyouadded&args=argumentsforyourscript
* Add more script types by editing scriptTypes.psrconfig (currently, PowerShell, Bash, PHP, and Node.js)